# Ablagesysteme <!-- omit in toc -->

# Applikationen und Daten trennen ... Wieso?
Stellen Sie sich vor, Sie müssten jedes Mal, wenn Sie ihre Wohnung betreten, gleich nach dem Öffnen der Wohnungstür den Wasserhahn im Bad öffnen und für 3 Minuten laufen lassen, weil sonst am nächsten Morgen der Supermarkt um die Ecke sein Rolltor nicht öffnen kann. Das Beispiel scheint völlig absurd, zudem gibt es überhaupt keinen ersichtlichen Zusammenhang zwischen den beiden Dingen. Gefriert sonst das Wasser und das Rolltor wird mit einer Wasserpumpe geöffnet? Aber was ist dann im Sommer? Und weshalb würde man so etwas bauen? Ist das Absicht? 
Lassen wir uns auf dieses absurde Beispiel ein und betrachten es unter den folgenden Gesichtspunkten:
 - **Interessen:** Es gibt direktes Interesse für den/die Bewohner(in) den Wasserhahn zu öffnen. Ein übergeordnetes Interesse (z.B. weil er sonst nicht einkaufen kann/darf oder weil der Ladenbesitzer mit Gewalt droht) müsste vorliegen, dass der/die Bewohner(in) den Wasserhahn pflichtbewusst bei jedem Betreten seiner Wohnung öffnet. 
 - **Abhängigkeit:** Zwischen dem Tor und dem Wasserhahn besteht eine einseitige Abhängigkeit. Die Hauptfunktion des Rolltores noch des Wasserhahnes lassen nicht auf einen solchen Zusammenhang schliessen. 
 - **Nicht intuitiv:** Besucht jemand anders die Wohnung so muss dieser wissen, dass er gleich beim Betreten den Wasserhahn aufdrehen muss. Wenn das Aufdrehen des Wasserhahns vergessen oder unterlassen wird, dann gibt es kein unmittelbares Signal. Und selbst es halb acht morgens ist und sich der Ladenbesitzer aufregt, weil der Anwohner schon wieder vergessen hat seinen Wasserhahn zu öffnen, und wütend zur dessen Wohnung marschiert, um den Anwohner zurechtzuweisen: Stellen Sie sich vor, Sie sind bei Ihrem besten Freund(in) zu Gast und jemand taucht an der Wohnungstüre auf und schnauzt Sie an, Sie sollen gefälligst ihren Wasserhahn öffnen. Vermutlich würden nicht wenige davon ausgehen, dass der Ladenbesitzer komplett irre ist. 

Man würde denken, dass solche Situationen nie vorkommen. Doch findet man in der Informatik zahlreiche solche Beispiele. Meist werden aus Bequemlichkeit (Faulheit) unnötige und gefährliche Abhängigkeiten geschaffen, die für Dritte nur schwer nachvollziehbar sind. Häufig bleiben solche Abhängigkeiten unbemerkt, bis eines Tages eine Komponente ausfällt und es zu unerklärlichen Aktionen oder Systemabstürzen kommt. 

In der Informatik findet man deshalb den Grundsatz [*low coupling, high cohesio*](https://medium.com/clarityhub/low-coupling-high-cohesion-3610e35ac4a6):
 - Module haben nur so viele Abhängigkeiten haben wie unbedingt notwendig sind.
 - "We want to design components that are self-contained: independent, and with a single, well-defined purpose"
 - "When you come across a problem, assess how localized the fix is. Do you change just one module, or are the changes scattered throughout the entire system? When you make a change, does it fix everything, or do other problems mysteriously arise?"

Wenn wir Applikationen und Daten trennen, indem wir unsere Dokumente unter ```C:\Users\meinname\Documents``` ablegen und die passenden Programme unter ```C:\Program Files\```, verfolgen wir die Ziele: wenig Kopplung und gute Wartbarkeit:
 - Bei einer Neuinstallation einer Software muss nicht darauf geachtet werden, dass aus Versehen Daten des Benutzers gelöscht werden. 
 - Daten können unabhängig von der Applikationen gesichert werden. 
 - Bessere Übersichtlichkeit: Benutzerdaten und Applikationsdaten sind eindeutig voneinander zu halten. 

# Übungen
 - [Wo speichere ich meine Daten ab](01_Wo%20speichere%20ich%20meine%20Daten%20ab.md)
 - [Eigenes Ablagekonzept](02_Eigenes%20Ablagekonzept.md)


# Ziele
 - Kann zwischen allgemeinen und personenbezogenen Daten unterscheiden.
 - B1G: Kann den Grundsatz Daten und Applikationen zu trennen erläutern
 - B1F: Wendet ein vorgegebenes Ablagekonzept an, um die eigenen Daten zu organisieren und zu speichern.
 - B1E: Entwirft ein eigenes Ablagekonzept für seine Daten und setzt dieses um.
 - B2G: Kann den Unterschied zwischen lokaler und cloudbasierter Ablage erläutern.
 - B2F: Richtet und setzt lokale und cloudbasierte Ablagen ein.
 - B2E: Vergleicht verschiedene cloudbasierte Ablagen.
