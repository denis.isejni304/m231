# Webseite verwendet Cookies

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Tandem |
| Aufgabenstellung  | Recherche zum Thema |
| Zeitbudget  |  1 Lektion |
| Ziele | Funktionsweise von Cookies erklären können, Herkunft und Zweck der Cookie Banner kennen |

Auftrag über die Hintergründe dieser Abfrage

## Einleitung
Sicher ist es Ihnen schon aufgefallen: Wenn Sie eine Schweizer Webseite aufrufen, werden Sie gefragt, ob Sie Cookies zulassen. Meist erhalten Sie irgendwo auf der Webseite einen Banner, der vielleicht so aussieht:
![Webseite verwendet Cookies Banner](media/WebseiteVerwendetCookies.png)

Quelle: https://www.kantonalbank.ch/de-CH/Home

## Auftrag
Suchen Sie nach Antworten auf die folgenden Fragen und dokumentieren Sie diese!

1. Je nach Webseite, welche Sie aufrufen, erhalten Sie unterschiedliche Optionen zur Auswahl <br> Welche Möglichkeiten finden Sie? Zeigen Sie verschiedene Beispiele!
2. Warum werden seit einiger Zeit solche Banner angezeigt? Seit wann?<br>Ist das nur auf Schweizer Websites der Fall?
3. Was sind Cookies? <br>Welche Arten von Cookies werden unterschieden? Wo kann man diese im Browser anschauen? <br>Braucht es Cookies? <br>Welche Ziele haben die Besitzer der Webseiten? <br>Was möchten Sie erreichen?<br> Warum?
4. Was haben diese Banner mit “Datenschutz” zu tun? 
<br>Worum geht es beim Datenschutz? Was sind “persönliche Daten”?
<br>Welche Reaktion auf die Banner würden Sie BenutzerInnen empfehlen? 
<br>Sollen bspw. immer alle akzeptiert werden?
5. Was passiert, wenn jemand eine Option akzeptiert hat? <br> Kann die Person Ihre Meinung danach wieder ändern? Wenn ja: Wie?
6. Welche gesetzlichen Vorgaben in Sachen Datenschutz gibt es für die Firmen? <br>Was heisst das für Sie, als Person, die in der Informatik arbeitet?
7. Einige Firmen werden mit dem Wort “Datenkraken” assoziiert. <br>
Nennen Sie Beispiele und Hintergründe!


## Hinweise
Beachten Sie folgende Dinge:
 - Arbeiten Sie im Tandem. 
 -  Begründen Sie jeweils Ihre Antworten möglichst genau. 
 - Formulieren Sie alle Texte (ausser natürlich Gesetztestexte und ähnliches) selbst. 
 - Eine direkte Übernahme von Text (bspw. aus dem Internet) gilt nicht als Antwort!
 - Arbeiten Sie mit Screenshots und Bildern. 
 - Geben Sie an, wo Sie die Beispiele gefunden haben (Quellen).
 - Strukturieren Sie den Text mit Titeln und Untertiteln.
 - Schreiben Sie am Ende eine kurze persönliche Reflexion über den Auftrag (einzeln!)

## Ressourcen
 - Wie du Cookies RICHTIG akzeptierst - [Video 7:28](https://www.youtube.com/watch?v=p4Y7l_RyZoM)
 - How Cookies can track you (Simply explained) - [Video 6:50](https://www.youtube.com/watch?v=QWw7Wd2gUJk)
 - How you are being tracked in the web | Online Tracking explained - [Video 7:57](https://www.youtube.com/watch?v=wefD2N-GWUo)
 - HTTP Cookies Crash Course - [Video 1:09:20](https://www.youtube.com/watch?v=sovAIX4doOE&t=3505s)