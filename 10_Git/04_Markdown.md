# Markdown Syntax kennenlernen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie sind in der Lage ein Dokument in Markdown zu gestalten.<br>Sie kennen die wichtigsten Markdown Elemente.<br> |

## Aufgabenstellung
Gestalten Sie ein Markdown Dokument, dass gerendert genauso aussieht wie «Markdown Example.html». Informieren Sie sich dafür selbstständig über den Markdown Syntax. 

## Links
 - https://www.markdownguide.org/basic-syntax/
 - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
