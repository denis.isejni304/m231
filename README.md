# M231 - Datenschutz und Datensicherheit anwenden

Setzt Datenschutz und Datensicherheit bei Informatiksystemen ein. Überprüft vorhandene Systeme auf Einhaltung von Richtlinien.

Eine Übersicht über die im Modul 231 zu erreichenden Kompetenzen finden Sie im Abschnitt [Kompetenzmatrix](00_kompetenzband/)

# Modulinhalt
 - [Datenschutz](01_Datenschutz/)
 - [Verschlüsselung](03_Passw%C3%B6rter/)
 - [Passwortverwaltung](03_Passw%C3%B6rter/)
 - [Ablagesysteme](04_Ablagesysteme/)
 - [Backup](05_Backup/)
 - [Git / Markdown](10_Git/)

# Leistungsbeurteilung

Alle Informationen zu der Leistungsbeurteilung finden Sie [hier](/99_Leistungsbeurteilung/). 
 
 # Unterlagen
  - Gitrepository [ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)
  - Gitbook [ch-tbz-it.gitlab.io/Stud/m231/](https://ch-tbz-it.gitlab.io/Stud/m231/)
 - Unterlagen die aus urheberrechtlichen Gründen nicht hier veröffentlicht werden können, sind im MS Teams zu finden. 

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.