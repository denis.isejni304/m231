# Einführung ins Thema Backup
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  1 Lektion |
| Ziele | Wie wissen was ein Backup ist und die wesentlichen Aspekte |

Informieren Sie sich *Herdt* Buch "Informationstechnologie Grundlagen (Stand 2021)" von Seite 216 bis 223 über die Grundlagen zum Thema Backup.

Themen:
 - Wohin sichern?
 - Datensicherungsstrategien
 - Schnittstellen

Tipp:
 - Wichtigste Punkte (Kernaussagen) auf einer Zusammenfassung festhalten.