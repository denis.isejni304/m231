
# 6. Regeln für sichere Passwörter
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung mit eigenen Notebook |
| Zeitbudget  |  3 Lektionen |
| Ziel | Herausforderungen von sicheren Passwörtern mithilfe einer praktischen Übung besser verstehen. |

## 6.1. Einleitung
Nicht selten verwenden User persönliche Informationen, wie Postleitzahlen, Autonummern, Name des Haustieres oder Name der/des Ehepartner*in als Passwort. Das ist nicht zwangsläufig unsicher, aber es wird insbesondere dann unsicher, wenn ausser diesen Informationen keine unspezifischen Elemente vorhanden sind. Bei Security Reviews von Firmen wird gerne geprüft, ob Mitarbeiter ihre Passwörter auf diese Weise gewählt haben. 

In dieser kleinen Übung werden Sie selbst einen solchen Review durchführen. Dafür stehen ihnen folgende Mittel und Tools zur Verfügung.
 - Fake-Profile der Firma und von den zwei Mitarbeiter
 - Fake-ERP der Strauss Fricke AG
 - Python Applikation

## 6.2. Voraussetzungen
Damit Sie diese Übung durchführen können, benötigen Sie folgende Tools auf Ihrem PC:
 - Git: https://git-scm.com/downloads
 - Python 3.* https://www.python.org/downloads/

## 6.3. Ziel
 - Finden Sie alle mögliche Logins heraus. Wenn Sie sich erfolgreich ins System eingeloggt haben, wird eine *secret sentence* angezeigt. Die Person oder das Team, dass als erstes diesen Satz in den Teams Chat postet, gewinnt. 

## 6.4. Vorgehen
 - Lesen Sie die gesamte Übung durch
 - Installieren Sie allenfalls fehlende Tools auf Ihrem Notebook
 - Gehen Sie auf https://github.com/alptbz/seleniumbruteforce und folgen Sie den Instruktionen
 - In den Instruktionen steht, dass Sie *words.txt* befüllen müssen. Das Python-Skript *password_generator.py* nimmt diese Wörter und generiert daraus alle möglichen Variationen.
```
Beispiel:
In words.txt fügen Sie zwei Worte ein:
eins
zwei

password_generator.py wird in dictionary.txt folgendes reinschreiben (Zusätzliche Gross- und KleinSchreibungsvarianten weggelassen):
eins
zwei
einseins
zweieins
einszwei
zweizwei
einseinseins
zweieinseins
einszweieins
zweizweieins
einseinszwei
zweieinszwei
einszweizwei
zweizweizwei
```
 - Studieren Sie die Fake-Profile. Welche Informationen könnten Sie für die Generierung des *password dictionaries* verwenden?
 - Schreiben Sie einzelnen Elemente in *words.txt*. Es könnte zum Beispiel so aussehen:
```
9056
schachen
1974
371372
```
 - Denken Sie daran, dass je mehr Informationen Sie einfügen, desto länger dauert das *Bruteforcing*. 
 - Die Benutzernamen werden nach dem Schema *[Erster Buchstabe Vorname].[Nachname]* gebildet (ohne Umlaute!)* Beispiel: Aus *Timo Müller* wird *t.mueller*

## 6.5. Tipps
Bei der Installation von Python müssen Sie unbedingt, *Add Python 3.9 to PATH* auswählen, damit Sie python von *git bash* aufrufen können. 
![Python Path](images/python_installation_tipp.PNG)
