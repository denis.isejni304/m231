# MFA - Multi-Faktor-Authentisierung
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  20 Minuten |
| Ziel | Mehrfaktor-Authentifizierung verstehen |

In der Einleitung haben sie sich mit den verschiedenen Möglichkeiten der Authentifizierung auseinandergesetzt.

Erarbeiten Sie sich ein Verständnis für Mehrfaktor-Authentifizierung mithilfe der nachfolgenden Beiträge:
 - Wikipedia zu Multi-Faktor-Authentisierung:
   - EN: https://en.wikipedia.org/wiki/Multi-factor_authentication 
   - DE: https://de.wikipedia.org/wiki/Multi-Faktor-Authentisierung
 - https://dis-blog.thalesgroup.com/security/2011/09/05/three-factor-authentication-something-you-know-something-you-have-something-you-are/
