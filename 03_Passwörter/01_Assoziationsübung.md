# 3. Assoziationsübung Thema Passwörter 
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Assoziationsübung |
| Zeitbudget  |  10 Minuten |
| Ziel | Vorwissen zum Thema "Passwörter" aktivieren.  |

Die Instruktionen zu dieser Aufgabe erhalten Sie von der Lehrperson. 
