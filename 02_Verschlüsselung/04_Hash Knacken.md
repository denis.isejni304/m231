# Hash Knacken
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Hash knacken  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1G: Kann das Prinzip der Verschlüsselung von Daten erläutern |

Versuchen Sie nachfolgende Hashes zu knacken (= Inhalt erraten)

| Hash  | Algorithmus | Hinweis |
|---|---|---|
| `b68a03c41c766d8c13e7a92ab18f7e58` | MD5 | Ohne Salz |
| `fcea6264406204ade8f24d41871f1eb3` | MD5 | Mit Salz (Selber Inhalt) |
| `0bf83ff3617d7ab2bc13223f61fbb6c2a851257553d009ee04186acee09d0f41` | SHA-256 | Ohne Salz |
| `d0f70aae4c886b6c7c121a2c420ed3b3b7ab7388ccc748152732bc1e55d727f7` | SHA-256 | Mit Salz |

Der Inhalt ist bei allen Hashes derselbe. 

Was steht da drin?

Wie viele der vier Hashes können Sie knacken?

Welche Methode haben Sie verwendet?
