# Verschlüsselung <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [3. Hash(funktion)](#3-hashfunktion)
  - [3.1. Aufgabe - Überprüfung heruntergeladenen *installation images (ISO)*](#31-aufgabe---überprüfung-heruntergeladenen-installation-images-iso)
    - [3.1.1. Videoanleitung - Hashwert einer Datei überprüfen](#311-videoanleitung---hashwert-einer-datei-überprüfen)
  - [3.2. Aufgabe - Gleicher Input gleicher Hash](#32-aufgabe---gleicher-input-gleicher-hash)
- [4. Veracrypt](#4-veracrypt)
  - [4.1. Aufgabenstellung](#41-aufgabenstellung)
  - [4.2. Hinweise](#42-hinweise)
  - [4.3. Videoanleitung - Veracrypt](#43-videoanleitung---veracrypt)
- [5. Cryptomator](#5-cryptomator)
  - [5.1. Aufgabenstellung](#51-aufgabenstellung)


