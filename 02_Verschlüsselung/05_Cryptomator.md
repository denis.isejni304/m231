# Cryptomator
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Verschlüsselter Container und Filehosting-Dienste einrichten  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1F: Richtet und setzt Verschlüsselungen für Daten ein. |

Dank DropBox, Google Drive, OneDrive, iCloud lassen sich Dateien im Handumdrehen über mehrere Geräte synchronisieren. Die meisten Filehosting-Dienste kosten für die ersten paar Gigabytes nichts und sind gut in allen gängigen Betriebssystemen implementiert. Das Problem: Die eigenen Dateien werden unverschlüsselt auf den Server des Providers gespeichert. Und selbst wenn der Provider die Dateien verschlüsselt, besitzt dieser die Schlüssel. 

Damit man von diesem Angeboten profitieren und dabei trotzdem seine Privatsphäre schützten kann, gibt es praktische Tools, wie der [Crytomator](https://cryptomator.org/), der es erlaubt verschlüsselte Container auf Filehosting-Dienste einzurichten. Im Vergleich zu Veracrypt hat der Container keine fixe Grösse und splitet die Verschlüsselten Daten in mehrere einzelne Files auf, sodass diese einfacher synchronisiert werden können. 

## Aufgabenstellung
Sofern Sie einen Filehosting-Dienste verwenden, laden Sie sich Cryptomator herunter und richten Sie sich ein verschlüsseltes Volumen ein. 

[Auf Youtube finden Sie dazu zahlreiche Videos.](https://www.youtube.com/results?search_query=cryptomator)