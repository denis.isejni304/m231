# Veracrypt
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Mit Veracrypt ein verschlüsselter Container erstellen.  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1F: Richtet und setzt Verschlüsselungen für Daten ein. |

"VeraCrypt ist eine Software zur Datenverschlüsselung, insbesondere zur vollständigen oder partiellen Verschlüsselung von Festplatten und Wechseldatenträgern." ([Wikipedia - Veracrypt](https://de.wikipedia.org/wiki/VeraCrypt))

Verschlüsselung soll sicherstellen, dass nur autorisierte Empfänger auf vertrauliche Daten, Informationen und Kommunikation zugreifen können. Üblicherweise wird Verschlüsselung für folgende Zwecke eingesetzt:
 - Kontinuierliche Verschlüsselung von Datenströmen (z.B. HTTPS bzw. SSL/TLS)
 - Verschlüsselung von Laufwerken und Container (z.B. Veracrypt, LUKS, Bitlocker)
 - Verschlüsselung von einzelnen Dateien oder Nachrichten (z.B. PGP, 7z (AES))

[Veracrypt](veracrypt.fr) fällt in den Verwendungszweck "Verschlüsselung von Laufwerken und Container". Im Vergleich zu der Verschlüsselung von einzelnen Dateien hat dieser Verwendungszweck den Vorteil, dass alle Dateien im Laufwerk automatisch verschlüsselt sind und nicht jede Datei durch aktive Betätigung des Benutzers einzeln verschlüsselt werden muss.

## Aufgabenstellung
Mit Veracrypt können Sie ganze Datenträger verschlüsseln. In dieser Übung verwenden wir jedoch nur die Funktion der Erstellung eines Containers. 

**Ziele der Aufgabe**:
 - Veracrypt auf dem eigenen Computer installieren.
 - Einen neuen mit Veracrypt erstellen.
 - Den Container mounten. 
 - Den neuen Container mit Dateien befüllen. 
 - Den Container schliessen und nochmals mounten.

## Hinweise
 - Ein Veracrypt-Container hat eine fixe Grösse. Das bedeutet, dass ein Container mit 5GB eine genauso grosse (.hc) Datei erstellt. Dieser Umstand setzt deshalb voraus, dass vor der Erstellung des Containers abgeschätzt werden muss, wie gross der Container sein muss, sodass alle zu verschlüsselnde Dateien darin Platz haben und das danach ein wenig Platz übrig bleibt. Letzteres ist notwendig, damit die Grösse des Containers nicht ständig angepasst werden muss. Bei der Wahl der Container-Grösse gilt der Grundsatz: *So klein wie möglich, aber so gross wie nötig.* 

## Videoanleitung - Veracrypt
In diesem Video werden alle Schritte gezeigt, die für das Erreichen der Aufgabenziele benötigt werden. 
![Videoanleitung - Veracrypt](videos/veracrypt.webm)